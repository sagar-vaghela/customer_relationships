class Log < ApplicationRecord
  belongs_to :user
  belongs_to :record
  enum state: [:created, :valued, :signed, :approved]  
end
