class CompanyHistory < ApplicationRecord
  belongs_to :user
  belongs_to :company

  # validates :cmp_mission, presence: true
   attr_accessor :role

  validates :cmp_mission, :cmp_vision, :cmp_values, :cmp_value_proposition, :cmp_business_plan, :cmp_hr_provider, :cmp_competitors, :cmp_organizational_structure, :number_of_workers, :work_environment_score, :workers_profile, :workers_average_age, :workers_average_time_in_company, :cmp_changes_in_last_year, :geographical_state, :geographical_region, :political_context, :economic_context, :social_context, :technological_context, :environmental_context, :legal_context, :relationship_with_client, :complexity_in_relationship_with_client, :primary_contact_name, :primary_contact_phone, :primary_contact_cellphone, :position_of_primary_contact, :primary_contact_email, :secondary_contact_name, :contact_position, :secondary_contact_email, :consultant_charge, :service_description, :service_complexities_while_offering, :score, :conditions, :last_service_provided_name, presence: true,  if: -> {role != "admin" }, on: :update

  validates_format_of :primary_contact_email, :secondary_contact_email, with: /\A.+@.+\..+\Z/i, message: 'is not valid',  if: -> {role != "admin" }, on: :update

end
