class Company < ApplicationRecord
  #association
  attr_accessor :user_ids

  has_many :company_users
  has_many :users, through: :company_users, dependent: :destroy
  has_many :records, dependent: :destroy
  belongs_to :created_by, class_name: 'Company', foreign_key: 'parent_id', optional: true
  has_many :created_companies, class_name: 'Company', foreign_key: 'parent_id'
  has_many :company_histories, dependent: :destroy
  enum cmp_size: [:small, :large]

  #includes
  has_attached_file :company_logo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :company_logo, content_type: ['image/jpeg', 'image/jpg', 'image/png']

  validates :cmp_name, presence: true
  validates :industry, presence: true
  validates :sub_industry, presence: true
  validates :user_ids, presence: true
  validates :parent_id, presence: true, if: -> {self.is_holding == true}
  validates_uniqueness_of :cmp_name

  def child_companies
    self.created_companies
  end
end
