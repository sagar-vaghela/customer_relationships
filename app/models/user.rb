class User < ApplicationRecord
  rolify
  attr_accessor :user_role
  attr_accessor :skip_password_validation

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :invitable

  has_many :company_users
  has_many :companies, through: :company_users, dependent: :destroy
  # has_many :companies, dependent: :destroy
  has_many :records, dependent: :destroy
  has_many :logs, dependent: :destroy


  validates :email, presence: true
  validates :first_name, presence: true, if: -> {self.reset_password_token.nil?}
  validates :last_name, presence: true, if: -> {self.reset_password_token.nil?}
  validates :user_role, presence: true, if: -> {self.reset_password_token.nil?}

  after_create :add_role_to_user
  after_create :send_invitation

  scope :admins, -> {joins(:roles).where("roles.name = 'Admin'")}
  scope :leaders, -> {joins(:roles).where("roles.name = 'Leader'")}
  scope :consultantants, -> {joins(:roles).where("roles.name = 'Consultant'")}

  def add_role_to_user
    self.add_role self.user_role if self.user_role.present?
  end

  def send_invitation
    if self.roles.first.name != 'Admin'
      self.invite!
    end
  end

  def role
    self.roles.first.name  if self.roles.present?
  end

  def to_s
    first_name.capitalize + " " +  last_name.capitalize  if first_name.present? && last_name.present?
  end

  protected

  def password_required?
    return false if skip_password_validation && self.user_role != 'admin'
    super
  end
end
