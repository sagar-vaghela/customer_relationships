require 'elasticsearch/model'
class Record < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  attr_accessor :user_role
  belongs_to :user
  belongs_to :company
  belongs_to :service
  belongs_to :process_table
  has_many :logs, dependent: :destroy

  enum state: [:created, :valued, :approved]
  enum survey_mode: [:paper, :web, :mix]
  enum service_duration_parameter: [:days, :months, :years]

  DAYS = %w(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31)
  MONTHS = %w(1 2 3 4 5 6 7 8 9 10 11 12)
  YEARS = %w(1 2 3 4 5 6 7 8 9 10)

  has_attached_file :record_pdf
  validates_attachment_content_type :record_pdf, content_type: ['application/pdf']

  validates :cmp_name, :cmp_general_managers_name, :cmp_hrm_name, :cmp_hrm_email, :official_name_of_client, :cmp_business_plan, :chilean_id_number, :cmp_address, :cmp_state, :cmp_city, :cmp_phone, :cmp_fax, :cmp_number, :cmp_office, :bill_receiver_name, :bill_receiver_phone, :primary_contact_person_name, :primary_contact_person_phone, :primary_contact_person_role, :primary_contact_person_email, :description_of_service, :user_id, :process_table_id,  presence: true

  validates :value_of_service, presence: true,  if: -> { self.user_role == "leader" || self.user_role == "admin"}, :numericality => true


  validates :cmp_phone_number, :presence => true, :numericality => true, :length => { :minimum => 10, :maximum => 15 }

  validates :number_of_employees, :affiliates_in_city, :affiliates_in_state, :affiliates_in_country, :presence => true, :numericality => true

  validates_format_of :primary_contact_person_email, :cmp_hrm_email, with: /\A.+@.+\..+\Z/i, message: 'is not valid'

end
