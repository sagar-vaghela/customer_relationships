class Service < ApplicationRecord
  has_many  :records, dependent: :destroy

  validates :name, presence: true
  validates_uniqueness_of :name

end
