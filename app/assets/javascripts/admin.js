$(function() {
  var flashCallback;
  flashCallback = function() {
      return $("#notice").fadeOut();
  };
  $("#notice").bind('click', (function(_this) {
      return function(ev) {
          return $("#notice").fadeOut();
      };
  })(this));
  return setTimeout(flashCallback, 2000);
});


$(function() {
  var flashCallback;
  flashCallback = function() {
      return $("#flash_alert").fadeOut();
  };
  $("#flash_alert").bind('click', (function(_this) {
      return function(ev) {
          return $("#flash_alert").fadeOut();
      };
  })(this));
  return setTimeout(flashCallback, 2000);
});



$(function() {
  var flashCallback;
  flashCallback = function() {
      return $("#flash_notice").fadeOut();
  };
  $("#flash_notice").bind('click', (function(_this) {
      return function(ev) {
          return $("#flash_notice").fadeOut();
      };
  })(this));
  return setTimeout(flashCallback, 2000);
});
