window.onload = function () {

	var chart = new CanvasJS.Chart("chartContainer", {
		animationEnabled: true,
		dataPointMaxWidth: 30,
		legend: {
			horizontalAlign: 'left',
	    verticalAlign: 'top',
	    floating: false,
	    fontSize: 14,
	    fontFamily: "'Lato', sans-serif",
	    fontWeight: "300",
	    color: "#e4e4e4"
		},
		axisX: {
			interval: 1,
			tickLength: 10,
			tickColor: "#fff",
			intervalType: "month",
			labelFontColor: "#777777",
			labelFontFamily: "'Lato', sans-serif",
			labelFontSize: 14,
			labelFontWeight: "300",
			lineColor: "#e4e4e4"
		},
		axisY:{
			valueFormatString:"#0",
			gridColor: "#e4e4e4",
			tickLength: 0,
			labelFontFamily: "'Lato', sans-serif",
			labelFontColor: "#777777",
			labelFontSize: 14,
			lineColor: "#e4e4e4",
			labelFontWeight: "300"
		},
		data: [
			{
				type: "stackedColumn",
				showInLegend: true,
				color: "#cccccc",
				name: "Signed",
				dataPoints: [
					{ label: "Jan", y: 60 },
					{ label: "Feb", y: 60 },
					{ label: "Mar", y: 60 },
					{ label: "Apr", y: 60 },
					{ label: "May", y: 60 },
					{ label: "Jun", y: 60 },
					{ label: "Jul", y: 60 },
					{ label: "Aug", y: 60 },
					{ label: "Sep", y: 60 },
					{ label: "Oct", y: 60 },				
					{ label: "Nov", y: 60 },				
					{ label: "Dec", y: 60 }				
				]
			},
			{        
				type: "stackedColumn",
				showInLegend: true,
				name: "Valued",
				color: "#a5c4e7",
				dataPoints: [
					{ label: "Jan", y: 20 },
					{ label: "Feb", y: 40 },
					{ label: "Mar", y: 60 },
					{ label: "Apr", y: 60 },
					{ label: "May", y: 60 },
					{ label: "Jun", y: 60 },
					{ label: "Jul", y: 60 },
					{ label: "Aug", y: 60 },
					{ label: "Sep", y: 60 },
					{ label: "Oct", y: 60 },				
					{ label: "Nov", y: 40 },				
					{ label: "Dec", y: 20 }
				]
			},
			{        
				type: "stackedColumn",
				showInLegend: true,
				name: "Created",
				color: "#a8cf96",
				dataPoints: [
					{ label: "Jan", y: 0 },
					{ label: "Feb", y: 0 },
					{ label: "Mar", y: 0 },
					{ label: "Apr", y: 20 },
					{ label: "May", y: 40 },
					{ label: "Jun", y: 60 },
					{ label: "Jul", y: 60 },
					{ label: "Aug", y: 40 },
					{ label: "Sep", y: 20 },
					{ label: "Oct", y: 0 },				
					{ label: "Nov", y: 0 },				
					{ label: "Dec", y: 0 }
				]
			}
		]
	});
	
	chartRender(chart);

	function chartRender(chart){
	  chart.render();
	  chart.ctx.shadowBlur = 9;
	  chart.ctx.shadowOffsetX = 15;
	  chart.ctx.shadowColor = "rgba(0, 0, 0, 0.15)";

	  for (var i = 0; i < chart.plotInfo.plotTypes.length; i++) {
      var plotType = chart.plotInfo.plotTypes[i];
      for (var j = 0; j < plotType.plotUnits.length; j++) {
        var plotUnit = plotType.plotUnits[j];
        //For Column Chart
        if (plotUnit.type === "column"){
          chart.renderColumn(plotUnit);
        }
        //For Bar Chart
        else if (plotUnit.type === "bar"){
          chart.renderBar(plotUnit);
        }
      }
	  }
	}

}