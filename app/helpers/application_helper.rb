module ApplicationHelper
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def show_errors(object, field_name, label)
    if object.errors.any?
      if !object.errors.messages[field_name].blank?
        "#{label} #{object.errors.messages[field_name][0]}"
      end
    end
  end

  def company_history_error(error, field_name)
    "#{field_name} #{error}"
  end

end
