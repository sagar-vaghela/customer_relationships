json.extract! admin_process, :id, :name, :created_at, :updated_at
json.url admin_process_url(admin_process, format: :json)
