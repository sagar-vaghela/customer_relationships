class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:sign_in, :password_new, :generate_new_password_email]
  before_action :user_side

  def user_side
    if current_user.present?
      if current_user.role == "Admin"
        redirect_to dashboard_page_admin_dashboard_index_path
      end
    end
  end

  def sign_in
    if current_user.present?
      if current_user.role == "Admin"
        redirect_to dashboard_page_admin_dashboard_index_path
      else
        redirect_to users_dashboard_page_path
      end
    end
  end

  def dashboard_page
    months = [1,2,3,4,5,6,7,8,9,10,11,12]
    @records = Record.all

    if current_user.role == 'Consultant'
      @records = @records.where(user_id: current_user.id)
    end

    @valued = Array.new
    @created = Array.new
    @approved = Array.new
    @pending = Array.new

    # begin
      # @elastic_records = Record.all
      # @elastic_records = @elastic_records.facets_search(params)

      if params[:state] || params[:year] || params[:service] || params[:consultant]
        @records = @records.where(state: params[:state]) if params[:state]
        @records = @records.where('extract(year  from date_of_service_provided) = ?', params[:year]) if params[:year]
        @records = @records.where('service_id = ?', params[:service]) if params[:service].present?
        @records = @records.where('user_id = ?', params[:consultant]) if params[:consultant].present?

        @service_id = params[:service].present? ? params[:service] : ''
        @user_id = params[:consultant].present? ? params[:consultant] : ''
        @year = params[:year].present? ? params[:year] : Time.now().strftime("%Y")



        months.each do |month|
          if params[:state].present?
            params[:state].each do |s|
              if s == "created"
                if current_user.role == 'Consultant'
                  if @service_id.present? && @user_id.present?
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  else
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  end
                elsif current_user.role == 'Leader'
                  if @service_id.present? && @user_id.present?
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, @user_id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ?', month, @year, @service_id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, @user_id).pluck(:value_of_service).sum
                  else
                    @created << @records.created.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, @year).pluck(:value_of_service).sum
                  end
                end
              end
              if s == "valued"
                if current_user.role == 'Consultant'
                  if @service_id.present? && @user_id.present?
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  else
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  end
                elsif current_user.role == 'Leader'
                  if @service_id.present? && @user_id.present?
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and service_id = ? and user_id = ?', month, @year, @service_id, @user_id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and service_id = ?', month, @year, @service_id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and user_id = ?', month, @year, @user_id).pluck(:value_of_service).sum
                  else
                    @valued << @records.valued.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, @year).pluck(:value_of_service).sum
                  end
                end
              end
              if s == "approved"
                if current_user.role == 'Consultant'
                  if @service_id.present? && @user_id.present?
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  else
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  end
                elsif current_user.role == 'Leader'
                  if @service_id.present? && @user_id.present?
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and service_id = ? and user_id = ?', month, @year, @service_id, @user_id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and service_id = ?', month, @year, @service_id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and user_id = ?', month, @year, @user_id).pluck(:value_of_service).sum
                  else
                    @approved << @records.approved.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, @year).pluck(:value_of_service).sum
                  end
                end
              end
              if s == "pending"
                if current_user.role == 'Consultant'
                  if @service_id.present? && @user_id.present?
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and service_id = ? and user_id = ?', month, @year, @service_id, current_user.id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  else
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, @year, current_user.id).pluck(:value_of_service).sum
                  end
                elsif current_user.role == 'Leader'
                  if @service_id.present? && @user_id.present?
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and service_id = ? and user_id = ?', month, @year, @service_id, @user_id).pluck(:value_of_service).sum
                  elsif @service_id.present?
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and service_id = ?', month, @year, @service_id).pluck(:value_of_service).sum
                  elsif @user_id.present?
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?  and user_id = ?', month, @year, @user_id).pluck(:value_of_service).sum
                  else
                    @pending << @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, @year).pluck(:value_of_service).sum
                  end
                end
              end
            end
          end
        end
      else
        months.each do |month|
          if current_user.role == 'Consultant'
            @created <<  @records.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, Time.now().strftime("%Y"), current_user.id ).pluck(:value_of_service).sum
            @valued <<  @records.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, Time.now().strftime("%Y"), current_user.id ).pluck(:value_of_service).sum
            @approved <<  @records.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, Time.now().strftime("%Y"), current_user.id ).pluck(:value_of_service).sum
            @pending <<  @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ? and user_id = ?', month, Time.now().strftime("%Y"), current_user.id ).pluck(:value_of_service).sum
          elsif current_user.role == 'Leader'
            @created <<  @records.search("created").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, Time.now().strftime("%Y") ).pluck(:value_of_service).sum
            @valued <<  @records.search("valued").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, Time.now().strftime("%Y") ).pluck(:value_of_service).sum
            @approved <<  @records.search("approved").records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, Time.now().strftime("%Y") ).pluck(:value_of_service).sum
            @pending <<  @records.search(["created","valued"]).records.where('extract(month from date_of_service_provided) = ? and extract(year  from date_of_service_provided) = ?', month, Time.now().strftime("%Y") ).pluck(:value_of_service).sum
          end
        end
      end

      respond_to do |format|
        request.format.html? ? format.html {} : format.js {}
      end
    end
    # rescue => ex
    #
    # end

  def generate_new_password_email
    user = User.find_by_email(params[:email])
    if user.present?
      # user.send_reset_password_instructions
      user.send_reset_password_instructions

      flash[:notice] = "Reset password instructions have been sent to #{user.email}."
    else
      flash[:alert] = "User not found for given email."
    end
    redirect_to root_path
  end

  def password_new
  end
end
