# frozen_string_literal: true

class SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    @user = User.find_by(email: params[:user][:email].downcase)
    if  @user && @user.valid_password?(params[:user][:password])
       # access token generation logic here
      if @user.active_for_authentication?
        if @user.role == 'Admin' && URI(request.referer).path == '/admins/sign_in'
          sign_in(@user, scope: :user)
          set_flash_message!(:notice, :signed_in)
          session[:user_id] = @user.id
          respond_with @user, location: after_sign_in_path_for(@user)
        elsif (@user.role == 'Leader' || @user.role == 'Consultant') && URI(request.referer).path != '/admins/sign_in'
          sign_in(@user, scope: :user)
          set_flash_message!(:notice, :signed_in)
          session[:user_id] = @user.id
          respond_with @user, location: after_sign_in_path_for(@user)
        else
          redirect_to request.referer, notice: "You are not to allowed login here"
        end
      end
    else
      redirect_to request.referer, alert: "Invalid username or password."
    end
  end

  # DELETE /resource/sign_out
  def destroy
    if current_user.role == 'Admin'
      sign_out current_user
      redirect_to new_user_session_path, notice: "You need to sign in before continuing."
    elsif current_user.role == 'Leader' || current_user.role == 'Consultant'
      sign_out current_user
      redirect_to root_path, notice: "You need to sign in before continuing."
    end
    # super
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
