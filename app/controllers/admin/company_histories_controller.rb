class Admin::CompanyHistoriesController < ApplicationController
  before_action :set_company_history, only: [:show, :edit, :update, :destroy]

  # GET /company_histories
  # GET /company_histories.json
  def index
    @company_histories = CompanyHistory.all
  end

  # GET /company_histories/1
  # GET /company_histories/1.json
  def show
  end

  # GET /company_histories/new
  def new
    @company_history = CompanyHistory.new
  end

  # GET /company_histories/1/edit
  def edit
  end

  # POST /company_histories
  # POST /company_histories.json
  def create
    @company_history = CompanyHistory.new(company_history_params)

    respond_to do |format|
      if @company_history.save
        format.html { redirect_to @company_history, notice: 'Company history was successfully created.' }
        format.json { render :show, status: :created, location: @company_history }
      else
        format.html { render :new }
        format.json { render json: @company_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company_histories/1
  # PATCH/PUT /company_histories/1.json
  def update
    respond_to do |format|
      if @company_history.update(company_history_params)
        format.html { redirect_to @company_history, notice: 'Company history was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_history }
      else
        format.html { render :edit }
        format.json { render json: @company_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company_histories/1
  # DELETE /company_histories/1.json
  def destroy
    @company_history.destroy
    respond_to do |format|
      format.html { redirect_to company_histories_url, notice: 'Company history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_history
      @company_history = CompanyHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_history_params
      params.require(:company_history).permit(:date_of_service_provided, :cmp_mission, :cmp_vision, :cmp_values, :cmp_value_proposition, :cmp_business_plan, :cmp_hr_provider, :cmp_competitors, :cmp_organizational_structure, :cmp_size, :number_of_workers, :work_environment_score, :workers_profile, :workers_average_age, :workers_average_time_in_company, :cmp_changes_in_last_year, :geographical_state, :geographical_region, :political_context, :economic_context, :social_context, :technological_context, :environmental_context, :legal_context, :relationship_with_client, :complexity_in_relationship_with_client, :primary_contact_name, :primary_contact_phone, :primary_contact_cellphone, :position_of_primary_contact, :primary_contact_email, :secondary_contact_name, :contact_position, :secondary_contact_email, :consultant_charge, :service_description, :service_complexities_while_offering, :score, :conditions, :last_service_provided_name, :service_start_date, :service_end_date, :user, :company, :year)
    end
end
