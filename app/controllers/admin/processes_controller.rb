class Admin::ProcessesController < ApplicationController
  before_action :set_admin_process, only: [:show, :edit, :update, :destroy]

  # GET /admin/processes
  # GET /admin/processes.json
  def index
    @processes = ProcessTable.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /admin/processes/1
  # GET /admin/processes/1.json
  def show
  end

  # GET /admin/processes/new
  def new
    @process = ProcessTable.new
  end

  # GET /admin/processes/1/edit
  def edit
  end

  # POST /admin/processes
  # POST /admin/processes.json
  def create
    @process = ProcessTable.new(process_params)

    respond_to do |format|
      if @process.save
        format.html { redirect_to admin_process_path(@process), notice: 'Process was successfully created.' }
        format.json { render :show, status: :created, location: @process }
      else
        format.html { render :new }
        format.json { render json: @process.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/processes/1
  # PATCH/PUT /admin/processes/1.json
  def update
    respond_to do |format|
      if @process.update(process_params)
        format.html { redirect_to admin_process_path(@process), notice: 'Process was successfully updated.' }
        format.json { render :show, status: :ok, location: @process }
      else
        format.html { render :edit }
        format.json { render json: @process.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/processes/1
  # DELETE /admin/processes/1.json
  def destroy
    @process.destroy
    respond_to do |format|
      format.html { redirect_to admin_processes_url, notice: 'Process was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_process
      @process = ProcessTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def process_params
      params.require(:process_table).permit(:name)
    end
end
