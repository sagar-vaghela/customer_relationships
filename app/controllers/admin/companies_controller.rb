class Admin::CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  load_and_authorize_resource

  # GET /admin/companies
  # GET /admin/companies.json
  def index
    @companies = Company.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /admin/companies/1
  # GET /admin/companies/1.json
  def show
    @company_histories = @company.company_histories
  end

  # GET /admin/companies/new
  def new
    @company = Company.new
  end

  # GET /admin/companies/1/edit
  def edit
  end

  # POST /admin/companies
  # POST /admin/companies.json
  def create
    @company = Company.new(company_params)
    respond_to do |format|
      # @company.company_histories.build(user_id: params[:company][:user_id], year: Time.now().strftime("%Y"))
      if @company.save
        @company.users << User.where(id: params[:company][:user_ids])
        format.html { redirect_to admin_company_path(@company), notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/companies/1
  # PATCH/PUT /admin/companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        @company.users << User.where(id: params[:company][:user_ids])
        format.html { redirect_to admin_company_path(@company), notice: 'Company was successfully updated.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/companies/1
  # DELETE /admin/companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to admin_companies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:cmp_name, :industry, :sub_industry, :is_holding, :parent_id, :company_logo, user_ids: [] )
    end
end
