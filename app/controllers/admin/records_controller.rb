class Admin::RecordsController < ApplicationController
  before_action :set_record, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /records
  # GET /records.json
  def index
    @records = Record.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /records/1
  # GET /records/1.json
  def show
  end

  # GET /records/new
  def new
    @record = Record.new
  end

  # GET /records/1/edit
  def edit
  end

  # POST /records
  # POST /records.json
  def create
    @record = Record.new(record_params)

    respond_to do |format|
      if @record.save
        format.html { redirect_to admin_record_path(@record), notice: 'Record was successfully created.' }
        format.json { render :show, status: :created, location: @record }
      else
        format.html { render :new }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /records/1
  # PATCH/PUT /records/1.json
  def update
    respond_to do |format|
      if @record.update(record_params)
        format.html { redirect_to admin_record_path(@record), notice: 'Record was successfully updated.' }
        format.json { render :show, status: :ok, location: @record }
      else
        format.html { render :edit }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record.destroy
    respond_to do |format|
      format.html { redirect_to admin_records_path, notice: 'Record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_record
      @record = Record.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def record_params
      params.require(:record).permit(:cmp_name, :cmp_phone_number, :cmp_general_managers_name, :cmp_hrm_name, :cmp_hrm_email, :official_name_of_client, :cmp_business_plan, :chilean_id_number, :cmp_address, :cmp_state, :cmp_city, :cmp_phone, :cmp_fax, :cmp_number, :cmp_office, :bill_receiver_name, :bill_receiver_phone, :number_of_employees, :affiliates_in_city, :affiliates_in_state, :affiliates_in_country, :primary_contact_person_name, :primary_contact_person_phone, :primary_contact_person_role, :primary_contact_person_email, :date_of_service_provided, :description_of_service, :value_of_service, :service_duration, :state, :user_id, :company_id, :number_of_segments, :survey_mode, :service_id, :service_duration_parameter, :user_role, :cmp_general_managers_email,  :service_conditions, :process_table_id)
    end
end
