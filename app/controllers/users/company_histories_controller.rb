class Users::CompanyHistoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_company_history, only: [:update]


  # POST /company_histories
  # POST /company_histories.json
  def create
    @company_history = CompanyHistory.new(company_id: params[:company],user_id: params[:user],year: params[:year])

    # @company_history = CompanyHistory.new(company_history_params)
    respond_to do |format|
      if @company_history.save
        format.html { redirect_to users_company_path(@company_history.company), notice: 'Company history was successfully created.' }
        format.json { render :show, status: :created, location: @company_history }
      else
        format.html {  redirect_to users_company_path(@company_history.company), notice: 'There is a problem in create.' }
        format.json { render json: @company_history.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /company_histories/1
  # PATCH/PUT /company_histories/1.json
  def update
    respond_to do |format|
      if @company_history.update(company_history_params)
        format.html { redirect_to users_company_path(@company_history.company), notice: 'Company history was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_history }
      else
        @company_history.role = current_user.role
        format.html { redirect_to users_company_path(@company_history.company), :flash => { :errors => @company_history.errors, :year => @company_history.year } }
        format.json { render json: @company_history.errors, status: :unprocessable_entity }
      end
    end
  end


  private

  # Use callbacks to share common setup or constraints between actions.
  def set_company_history
    @company_history = CompanyHistory.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def company_history_params
    params.require(:company_history).permit(:date_of_service_provided, :cmp_mission, :cmp_vision, :cmp_values, :cmp_value_proposition, :cmp_business_plan, :cmp_hr_provider, :cmp_competitors, :cmp_organizational_structure, :cmp_size, :number_of_workers, :work_environment_score, :workers_profile, :workers_average_age, :workers_average_time_in_company, :cmp_changes_in_last_year, :geographical_state, :geographical_region, :political_context, :economic_context, :social_context, :technological_context, :environmental_context, :legal_context, :relationship_with_client, :complexity_in_relationship_with_client, :primary_contact_name, :primary_contact_phone, :primary_contact_cellphone, :position_of_primary_contact, :primary_contact_email, :secondary_contact_name, :contact_position, :secondary_contact_email, :consultant_charge, :service_description, :service_complexities_while_offering, :score, :conditions, :last_service_provided_name, :service_start_date, :service_end_date, :user, :company, :year)
  end

end
