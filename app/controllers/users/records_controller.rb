class Users::RecordsController < ApplicationController

  before_action :set_record, only: [:edit, :update, :download_pdf, :record_pdf, :download]
  before_action :authenticate_user!

  # GET /records
  # GET /records.json
  # def index
  #   @records = Record.all
  # end

  # GET /records/1
  # GET /records/1.json
  # def show
  # end

  # GET /records/new
  def new
    @company = Company.find(params[:company])
    # @record = Record.new
    @record = @company.records.build
    @company_id = @company.id
  end

  # GET /records/1/edit
  def edit
  end

  # POST /records
  # POST /records.json
  def create
    @record = Record.new(record_params)
    puts @record.as_json
    puts @record.company_id
    @log = @record.logs.build(state: params[:record][:state], user_id: current_user.id) if params[:record][:state].present?
    respond_to do |format|
      if @record.save
        format.html { redirect_to users_company_path(@record.company), notice: 'Record was successfully created.' }
        format.json { render :show, status: :created, location: @record }
      else
        @company_id = @record.company_id
        format.html { render :new }
        # format.html { render action: :new, company: @record.company_id }
        # format.html { render new, company: @record.company_id, json: @record.errors }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /records/1
  # PATCH/PUT /records/1.json
  def update
    @log = @record.logs.build(state: params[:record][:state], user_id: current_user.id) if params[:record][:state].present?
    respond_to do |format|
      if @record.update(record_params)
        format.html { redirect_to users_company_path(@record.company), notice: 'Record was successfully updated.' }
        format.json { render :show, status: :ok, location: @record }
      else
        # format.html { redirect_to users_company_path(@record.company), notice: "There is a problem in update." }
        format.html { render :edit }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record.destroy
    respond_to do |format|
      format.html { redirect_to admin_records_path, notice: 'Record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def download_pdf
    html = render_to_string(:action => :record_pdf,id: params[:id], :layout => false)
    pdf = WickedPdf.new.pdf_from_string(html)
    send_data(pdf,:filename    => "records_#{@record.id}.pdf", type: :pdf)
  end

  def record_pdf
  end

  def download
    send_file @record.record_pdf.path,
    :filename => @record.record_pdf_file_name,
    :type => @record.record_pdf_content_type,
    :disposition => 'attachment'
    flash[:notice] = "Your file has been downloaded"
  end

  def update_log
    @log = Log.find(params[:log_id]) if params[:log_id].present?
    @log.update(description: params[:description])
    respond_to do |format|
      request.format.html? ? format.html {} : format.js {}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_record
      @record = Record.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def record_params
      params.require(:record).permit(:cmp_name, :cmp_phone_number, :cmp_general_managers_name, :cmp_hrm_name, :cmp_hrm_email, :official_name_of_client, :cmp_business_plan, :chilean_id_number, :cmp_address, :cmp_state, :cmp_city, :cmp_phone, :cmp_fax, :cmp_number, :cmp_office, :bill_receiver_name, :bill_receiver_phone, :number_of_employees, :affiliates_in_city, :affiliates_in_state, :affiliates_in_country, :primary_contact_person_name, :primary_contact_person_phone, :primary_contact_person_role, :primary_contact_person_email, :date_of_service_provided, :description_of_service, :value_of_service, :service_duration, :state, :user_id, :company_id, :name, :number_of_segments, :survey_mode, :record_pdf, :service_id, :service_duration_parameter, :cmp_general_managers_email, :service_conditions, :user_role, :process_table_id)
    end

end
