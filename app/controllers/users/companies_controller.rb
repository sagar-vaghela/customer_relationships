class Users::CompaniesController < ApplicationController
  before_action :authenticate_user!
  before_action :get_companies
  before_action :set_company, only: [:show, :change_year]
  before_action :user_side


  def user_side
    if current_user.role == "Admin"
      redirect_to dashboard_page_admin_dashboard_index_path
    end
  end


  def index
    if current_user.role == 'Consultant'
      # @companies = @companies.where(user_id: current_user.id)
      @companies = @companies.includes(:users).where('users.id = ?', current_user.id).references(:users)

    end
    @companies = @companies.paginate(:page => params[:page], :per_page => 8)
  end

  def show
    if flash['year'].present?
      @working_year = flash['year']
    else
      @working_year = Time.now().strftime("%Y")
    end

    if current_user.role == "Consultant"
      @company_histories = @company_histories.where(user_id: current_user.id)
      @records = @records.where(user_id: current_user.id)
    end

    if !@company_histories.present?
      @company_history = CompanyHistory.create(user_id: current_user.id, company_id: @company.id, year: Time.now().strftime("%Y"))
      @company_histories = CompanyHistory.where(company_id: @company.id)
    end

    @company_histories_years = @company_histories.order(:year).pluck(:year)
    @records_years = @records.where.not(date_of_service_provided: nil).all.order("date_of_service_provided").map {|d| d.date_of_service_provided.strftime("%Y").to_i }.uniq
    if @records_years.any?
      @years = (@records_years + @company_histories_years).uniq.sort
    else
      @years = (@company_histories_years).uniq.sort
    end

    @current_company_history = @company_histories.find_by(year: @working_year)
  end

  def change_year
    if current_user.role == 'Consultant'
      @company_histories = @company_histories.where(user_id: current_user.id)
    end
    @current_company_history = @company_histories.find_by(year: params[:year])

    if !@current_company_history.present?
      @current_company_history = CompanyHistory.create!(user_id: @company.user_id, company_id: @company.id, year: params[:year])
    end

    respond_to do |format|
      format.js
    end
  end

  protected

  def set_company
    @company = Company.find(params[:id])
    @company_histories = @company.company_histories
    @records = @company.records.order("date_of_service_provided")
  end

  def get_companies
    @companies = Company.all
  end

end
