class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :layout

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      # format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to main_app.root_url, notice: exception.message }
      # format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:accept_invitation, keys: [:first_name, :last_name, :user_role])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :user_role])
  end

  def after_sign_in_path_for(resource)
    if resource.roles.first.name == 'Admin'
      dashboard_page_admin_dashboard_index_path
    elsif resource.roles.first.name == 'Leader' || resource.roles.first.name == 'Consultant'
      users_dashboard_page_path
    end
  end

  private

  def layout
    # if devise controller then use login layout otherwise use application layout
    # is_a?(Devise::SessionsController) ? "login" : "application"
    if (is_a?(Devise::SessionsController) || is_a?(PasswordsController))
      "login"
    elsif(is_a?(UsersController) || is_a?(InvitationsController))
      "login_client"
    elsif current_user.present? && current_user.role == 'Admin'
      "application"
    elsif current_user.present? && current_user.role == 'Leader' || current_user.role == 'Consultant'
      "login_client"
    end
    # if is_a?(Devise::SessionsController)
    #   "login"
    # elsif is_a?(Devise::PasswordsController)
    #   "login_client"
    # elsif(is_a?(UsersController))
    #   "login_client"
    # else
    #   "application"
    # end
  end
end
