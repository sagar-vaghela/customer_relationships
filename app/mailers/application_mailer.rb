class ApplicationMailer < ActionMailer::Base
  default from: 'narola.ror@gmail.com'
  layout 'mailer'
end
