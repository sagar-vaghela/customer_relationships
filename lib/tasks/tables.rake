require 'csv'
namespace :tables do
  task company: :environment do
    csv_file = File.join(Rails.root, "company.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      Company.create(row.to_hash)
    end
  end
  task user_role: :environment do
    csv_file = File.join(Rails.root, "user_role.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      user_role.create(row.to_hash)
    end
  end
  task user: :environment do
    csv_file = File.join(Rails.root, "user.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      user = User.new(row.to_hash)
      user.skip_password_validation = true
      user.save
    end
  end
  task company_user: :environment do
    csv_file = File.join(Rails.root, "company_user.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      company_user = CompanyUser.new(row.to_hash)
      company_user.save(validate: false)
    end
  end
  task role: :environment do
    csv_file = File.join(Rails.root, "role.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      role = Role.new(row.to_hash)
      role.save(validate: false)
    end
  end

  task user_type: :environment do
    csv_file = File.join(Rails.root, "user_type.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      user_type = UserType.new(row.to_hash)
      user_type.save(validate: false)
    end
  end

  task service: :environment do
    csv_file = File.join(Rails.root, "service.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      service = Service.new(row.to_hash)
      service.save(validate: false)
    end
  end

  task complexity: :environment do
    csv_file = File.join(Rails.root, "complexity.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      complexity = Complexity.new(row.to_hash)
      complexity.save(validate: false)
    end
  end

  task process_table: :environment do
    csv_file = File.join(Rails.root, "process.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      process_table = ProcessTable.new(row.to_hash)
      process_table.save(validate: false)
    end
  end

  task record: :environment do
    csv_file = File.join(Rails.root, "record.csv")
    csv_text = File.read(csv_file)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "#{row.to_hash}"
      record = Record.new(row.to_hash)
      record.save(validate: false)
    end
  end

end
