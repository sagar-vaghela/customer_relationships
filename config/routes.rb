Rails.application.routes.draw do

  root 'users#sign_in'
  get 'users/dashboard_page'

  namespace :admin do
    resources :dashboard, only: [] do
      get :dashboard_page, on: :collection
    end
    resources :users
    resources :companies
    resources :records
    resources :company_histories, only: [:show]
    resources :services
    resources :processes
  end

  namespace :users do
    get :password_new
    post :generate_new_password_email
    resources :companies, only: [:index, :show] do
      get :change_year, on: :collection
    end
    resources :company_histories, only: [:update, :create, :new]
    resources :records, only: [:edit, :update, :create, :new] do
      get :download_pdf, on: :member
      get :record_pdf, on: :member
      get :download, on: :member
      patch :update_log, on: :collection
    end
  end

  devise_for :users, path: 'admins', :controllers => { :invitations => 'invitations', :sessions => 'sessions', :passwords => 'passwords'}

  # devise_for :admins , path: 'admins', :controllers => { :invitations => 'invitations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
