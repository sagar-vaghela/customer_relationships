class CreateRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :records do |t|
      t.string :cmp_name
      t.string :cmp_phone_number
      t.string :cmp_general_managers_name
      t.string :cmp_hrm_name
      t.string :cmp_hrm_email
      t.string :official_name_of_client
      t.string :cmp_business_plan
      t.string :chilean_id_number
      t.string :cmp_address
      t.string :cmp_state
      t.string :cmp_city
      t.string :cmp_phone
      t.string :cmp_fax
      t.string :cmp_number
      t.string :cmp_office
      t.string :bill_receiver_name
      t.string :bill_receiver_phone
      t.integer :number_of_employees
      t.string :affiliates_in_city
      t.string :affiliates_in_state
      t.string :affiliates_in_country
      t.string :primary_contact_person_name
      t.string :primary_contact_person_phone
      t.string :primary_contact_person_role
      t.string :primary_contact_person_email
      t.string :date_of_service_provided
      t.string :description_of_service
      t.decimal :value_of_service
      t.string :service_duration
      t.integer :state
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
