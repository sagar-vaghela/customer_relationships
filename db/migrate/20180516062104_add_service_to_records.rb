class AddServiceToRecords < ActiveRecord::Migration[5.2]
  def change
    add_reference :records, :service, foreign_key: true
  end
end
