class AddFieldsToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :is_holding, :boolean, null: false, default: false
    add_column :companies, :service_start_date, :date
    add_column :companies, :service_end_date, :date
    add_reference :companies, :parent, index: true, null: true, default: true
  end
end
