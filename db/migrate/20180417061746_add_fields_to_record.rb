class AddFieldsToRecord < ActiveRecord::Migration[5.1]
  def change
    add_column :records, :service_type, :string
    add_column :records, :number_of_segments, :integer
    add_column :records, :survey_mode, :integer
  end
end
