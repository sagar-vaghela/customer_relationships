class CreateCompanyHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :company_histories do |t|
      t.date :date_of_service_provided
      t.string :cmp_mission
      t.string :cmp_vision
      t.string :cmp_values
      t.string :cmp_value_proposition
      t.string :cmp_business_plan
      t.string :cmp_hr_provider
      t.string :cmp_competitors
      t.string :cmp_organizational_structure
      t.integer :cmp_size
      t.integer :number_of_workers
      t.float :work_environment_score
      t.string :workers_profile
      t.float :workers_average_age
      t.float :workers_average_time_in_company
      t.string :cmp_changes_in_last_year
      t.string :geographical_state
      t.string :geographical_region
      t.string :political_context
      t.string :economic_context
      t.string :social_context
      t.string :technological_context
      t.string :environmental_context
      t.string :legal_context
      t.string :relationship_with_client
      t.string :complexity_in_relationship_with_client
      t.string :primary_contact_name
      t.string :primary_contact_phone
      t.string :primary_contact_cellphone
      t.string :position_of_primary_contact
      t.string :primary_contact_email
      t.string :secondary_contact_name
      t.string :contact_position
      t.string :secondary_contact_email
      t.decimal :consultant_charge
      t.text :service_description
      t.text :service_complexities_while_offering
      t.string :score
      t.text :conditions
      t.string :last_service_provided_name
      t.date :service_start_date
      t.date :service_end_date
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
