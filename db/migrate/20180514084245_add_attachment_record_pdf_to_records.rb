class AddAttachmentRecordPdfToRecords < ActiveRecord::Migration[5.2]
  def self.up
    change_table :records do |t|
      t.attachment :record_pdf
    end
  end

  def self.down
    remove_attachment :records, :record_pdf
  end
end
