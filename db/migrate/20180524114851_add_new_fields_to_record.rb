class AddNewFieldsToRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :is_active, :integer
    add_column :records, :survey_name, :string
    add_column :records, :process_id, :integer
  end
end
