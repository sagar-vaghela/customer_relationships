class AddYearFieldToCompanyHistory < ActiveRecord::Migration[5.2]
  def change
    add_column :company_histories, :year, :integer
  end
end
