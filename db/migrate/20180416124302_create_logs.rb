class CreateLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :logs do |t|
      t.integer :state
      t.string :description
      t.references :user, foreign_key: true
      t.references :record, foreign_key: true

      t.timestamps
    end
  end
end
