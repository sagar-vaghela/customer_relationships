class AddNewFieldsToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :old_id, :string
    add_column :companies, :list_name, :string
    add_column :companies, :complexity_level_id, :integer
    add_column :companies, :image_path, :string
  end
end
