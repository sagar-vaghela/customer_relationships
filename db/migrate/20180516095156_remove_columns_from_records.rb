class RemoveColumnsFromRecords < ActiveRecord::Migration[5.2]
  def change
    remove_column :records, :affiliates_in_city, :string
    remove_column :records, :affiliates_in_state, :string
    remove_column :records, :affiliates_in_country, :string
    remove_column :records, :service_duration, :string
  end
end
