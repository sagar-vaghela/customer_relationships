class AddGmEmailToRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :cmp_general_managers_email, :string
    add_column :records, :service_conditions, :string
  end
end
