class AddColumnProcessTableIdToRecord < ActiveRecord::Migration[5.2]
  def change
    add_reference :records, :process_table, foreign_key: true
  end
end
