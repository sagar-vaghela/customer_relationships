class AddNameToService < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :name, :string
    add_column :services, :description, :text
    add_column :services, :is_millestone, :text
    add_column :services, :has_period, :int
  end
end
