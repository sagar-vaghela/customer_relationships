class ChangeDefautvalueForParentId < ActiveRecord::Migration[5.2]
  def up
    change_column :companies, :parent_id, :integer, index: true, null: true, default: false
  end
  def down
    change_column :companies, :parent_id, :integer, index: true, null: true, default: true  
  end
end
