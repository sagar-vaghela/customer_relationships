class RemoveUserFromCompany < ActiveRecord::Migration[5.2]
  def self.up
    change_table :companies do |t|
      t.remove :user_id
    end
  end

  def self.down
    change_table :companies do |t|
      t.references :user
    end
  end
end
