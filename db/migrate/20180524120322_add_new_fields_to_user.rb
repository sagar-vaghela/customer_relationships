class AddNewFieldsToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :token, :string
    add_column :users, :active, :integer
  end
end
