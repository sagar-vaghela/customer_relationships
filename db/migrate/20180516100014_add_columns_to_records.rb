class AddColumnsToRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :affiliates_in_city, :integer
    add_column :records, :affiliates_in_state, :integer
    add_column :records, :affiliates_in_country, :integer
    add_column :records, :service_duration, :integer
    add_column :records, :service_duration_parameter, :integer
  end
end
