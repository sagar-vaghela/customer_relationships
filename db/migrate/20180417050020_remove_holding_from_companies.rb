class RemoveHoldingFromCompanies < ActiveRecord::Migration[5.1]
  def change
    remove_column :companies, :holding, :string
  end
end
