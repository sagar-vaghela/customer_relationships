class ChangeDatatypeForDateOfServiceProvidedField < ActiveRecord::Migration[5.2]
  def up
    change_column :records, :date_of_service_provided, :date
  end
  def down
    change_column :records, :date_of_service_provided, :string
  end
end
