class CreateProcessTables < ActiveRecord::Migration[5.2]
  def change
    create_table :process_tables do |t|
      t.string :name

      t.timestamps
    end
  end
end
