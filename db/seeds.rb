# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Role.find_or_create_by(name: 'Admin')
Role.find_or_create_by(name: 'Consultant')
Role.find_or_create_by(name: 'Leader')
@user = User.find_by_email('shm@narola.email')
unless @user.present?
  @user = User.new(email: 'shm@narola.email', first_name: 'shantilal', last_name: 'narola', user_role: 'Admin',  password: 'password')
  @user.roles << Role.find_by_name('Admin')
  @user.save
end
