# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_04_18_100443) do

  create_table "companies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "cmp_name"
    t.date "date_of_service_provided"
    t.string "industry"
    t.string "sub_industry"
    t.string "cmp_mission"
    t.string "cmp_vision"
    t.string "cmp_values"
    t.string "cmp_value_proposition"
    t.string "cmp_business_plan"
    t.string "cmp_hr_provider"
    t.string "cmp_competitors"
    t.string "cmp_organizational_structure"
    t.integer "cmp_size"
    t.integer "number_of_workers"
    t.float "work_environment_score"
    t.string "workers_profile"
    t.float "workers_average_age"
    t.float "workers_average_time_in_company"
    t.string "cmp_changes_in_last_year"
    t.string "geographical_state"
    t.string "geographical_region"
    t.string "political_context"
    t.string "economic_context"
    t.string "social_context"
    t.string "technological_context"
    t.string "environmental_context"
    t.string "legal_context"
    t.string "relationship_with_client"
    t.string "complexity_in_relationship_with_client"
    t.string "primary_contact_name"
    t.string "primary_contact_phone"
    t.string "primary_contact_cellphone"
    t.string "position_of_primary_contact"
    t.string "primary_contact_email"
    t.string "secondary_contact_name"
    t.string "contact_position"
    t.string "secondary_contact_email"
    t.decimal "consultant_charge", precision: 10
    t.string "service_description"
    t.string "service_complexities_while_offering"
    t.string "score"
    t.string "conditions"
    t.string "last_service_provided_name"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_holding", default: false, null: false
    t.date "service_start_date"
    t.date "service_end_date"
    t.bigint "parent_id", default: 1
    t.index ["parent_id"], name: "index_companies_on_parent_id"
    t.index ["user_id"], name: "index_companies_on_user_id"
  end

  create_table "logs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "state"
    t.string "description"
    t.bigint "user_id"
    t.bigint "record_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_id"], name: "index_logs_on_record_id"
    t.index ["user_id"], name: "index_logs_on_user_id"
  end

  create_table "records", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "cmp_name"
    t.string "cmp_phone_number"
    t.string "cmp_general_managers_name"
    t.string "cmp_hrm_name"
    t.string "cmp_hrm_email"
    t.string "official_name_of_client"
    t.string "cmp_business_plan"
    t.string "chilean_id_number"
    t.string "cmp_address"
    t.string "cmp_state"
    t.string "cmp_city"
    t.string "cmp_phone"
    t.string "cmp_fax"
    t.string "cmp_number"
    t.string "cmp_office"
    t.string "bill_receiver_name"
    t.string "bill_receiver_phone"
    t.integer "number_of_employees"
    t.string "affiliates_in_city"
    t.string "affiliates_in_state"
    t.string "affiliates_in_country"
    t.string "primary_contact_person_name"
    t.string "primary_contact_person_phone"
    t.string "primary_contact_person_role"
    t.string "primary_contact_person_email"
    t.string "date_of_service_provided"
    t.string "description_of_service"
    t.decimal "value_of_service", precision: 10
    t.string "service_duration"
    t.integer "state"
    t.bigint "user_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "service_type"
    t.integer "number_of_segments"
    t.integer "survey_mode"
    t.index ["company_id"], name: "index_records_on_company_id"
    t.index ["user_id"], name: "index_records_on_user_id"
  end

  create_table "roles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: ""
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.integer "invited_by_id"
    t.string "invited_by_type"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "companies", "users"
  add_foreign_key "logs", "records"
  add_foreign_key "logs", "users"
  add_foreign_key "records", "companies"
  add_foreign_key "records", "users"
end
