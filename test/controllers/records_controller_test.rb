require 'test_helper'

class RecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @record = records(:one)
  end

  test "should get index" do
    get records_url
    assert_response :success
  end

  test "should get new" do
    get new_record_url
    assert_response :success
  end

  test "should create record" do
    assert_difference('Record.count') do
      post records_url, params: { record: { affiliates_in_city: @record.affiliates_in_city, affiliates_in_country: @record.affiliates_in_country, affiliates_in_state: @record.affiliates_in_state, bill_receiver_name: @record.bill_receiver_name, bill_receiver_phone: @record.bill_receiver_phone, chilean_id_number: @record.chilean_id_number, cmp_address: @record.cmp_address, cmp_business_plan: @record.cmp_business_plan, cmp_city: @record.cmp_city, cmp_fax: @record.cmp_fax, cmp_general_managers_name: @record.cmp_general_managers_name, cmp_hrm_email: @record.cmp_hrm_email, cmp_hrm_name: @record.cmp_hrm_name, cmp_name: @record.cmp_name, cmp_number: @record.cmp_number, cmp_office: @record.cmp_office, cmp_phone: @record.cmp_phone, cmp_phone_number: @record.cmp_phone_number, cmp_state: @record.cmp_state, company_id: @record.company_id, date_of_service_provided: @record.date_of_service_provided, description_of_service: @record.description_of_service, number_of_employees: @record.number_of_employees, number_of_segments: @record.number_of_segments, official_name_of_client: @record.official_name_of_client, primary_contact_person_email: @record.primary_contact_person_email, primary_contact_person_name: @record.primary_contact_person_name, primary_contact_person_phone: @record.primary_contact_person_phone, primary_contact_person_role: @record.primary_contact_person_role, service_duration: @record.service_duration, service_type: @record.service_type, state: @record.state, survey_mode: @record.survey_mode, user_id: @record.user_id, value_of_service: @record.value_of_service } }
    end

    assert_redirected_to record_url(Record.last)
  end

  test "should show record" do
    get record_url(@record)
    assert_response :success
  end

  test "should get edit" do
    get edit_record_url(@record)
    assert_response :success
  end

  test "should update record" do
    patch record_url(@record), params: { record: { affiliates_in_city: @record.affiliates_in_city, affiliates_in_country: @record.affiliates_in_country, affiliates_in_state: @record.affiliates_in_state, bill_receiver_name: @record.bill_receiver_name, bill_receiver_phone: @record.bill_receiver_phone, chilean_id_number: @record.chilean_id_number, cmp_address: @record.cmp_address, cmp_business_plan: @record.cmp_business_plan, cmp_city: @record.cmp_city, cmp_fax: @record.cmp_fax, cmp_general_managers_name: @record.cmp_general_managers_name, cmp_hrm_email: @record.cmp_hrm_email, cmp_hrm_name: @record.cmp_hrm_name, cmp_name: @record.cmp_name, cmp_number: @record.cmp_number, cmp_office: @record.cmp_office, cmp_phone: @record.cmp_phone, cmp_phone_number: @record.cmp_phone_number, cmp_state: @record.cmp_state, company_id: @record.company_id, date_of_service_provided: @record.date_of_service_provided, description_of_service: @record.description_of_service, number_of_employees: @record.number_of_employees, number_of_segments: @record.number_of_segments, official_name_of_client: @record.official_name_of_client, primary_contact_person_email: @record.primary_contact_person_email, primary_contact_person_name: @record.primary_contact_person_name, primary_contact_person_phone: @record.primary_contact_person_phone, primary_contact_person_role: @record.primary_contact_person_role, service_duration: @record.service_duration, service_type: @record.service_type, state: @record.state, survey_mode: @record.survey_mode, user_id: @record.user_id, value_of_service: @record.value_of_service } }
    assert_redirected_to record_url(@record)
  end

  test "should destroy record" do
    assert_difference('Record.count', -1) do
      delete record_url(@record)
    end

    assert_redirected_to records_url
  end
end
