require 'test_helper'

class Admin::CompaniesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_company = admin_companies(:one)
  end

  test "should get index" do
    get admin_companies_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_company_url
    assert_response :success
  end

  test "should create admin_company" do
    assert_difference('Admin::Company.count') do
      post admin_companies_url, params: { admin_company: { cmp_business_plan: @admin_company.cmp_business_plan, cmp_changes_in_last_year: @admin_company.cmp_changes_in_last_year, cmp_competitors: @admin_company.cmp_competitors, cmp_hr_provider: @admin_company.cmp_hr_provider, cmp_mission: @admin_company.cmp_mission, cmp_name: @admin_company.cmp_name, cmp_organizational_structure: @admin_company.cmp_organizational_structure, cmp_size: @admin_company.cmp_size, cmp_value_proposition: @admin_company.cmp_value_proposition, cmp_values: @admin_company.cmp_values, cmp_vision: @admin_company.cmp_vision, complexity_in_relationship_with_client: @admin_company.complexity_in_relationship_with_client, conditions: @admin_company.conditions, consultant_charge: @admin_company.consultant_charge, contact_position: @admin_company.contact_position, created_at: @admin_company.created_at, date_of_service_provided: @admin_company.date_of_service_provided, economic_context: @admin_company.economic_context, environmental_context: @admin_company.environmental_context, geographical_region: @admin_company.geographical_region, geographical_state: @admin_company.geographical_state, industry: @admin_company.industry, integer: @admin_company.integer, is_holding: @admin_company.is_holding, last_service_provided_name: @admin_company.last_service_provided_name, legal_context: @admin_company.legal_context, number_of_workers: @admin_company.number_of_workers, parent_id: @admin_company.parent_id, political_context: @admin_company.political_context, position_of_primary_contact: @admin_company.position_of_primary_contact, primary_contact_cellphone: @admin_company.primary_contact_cellphone, primary_contact_email: @admin_company.primary_contact_email, primary_contact_name: @admin_company.primary_contact_name, primary_contact_phone: @admin_company.primary_contact_phone, relationship_with_client: @admin_company.relationship_with_client, score: @admin_company.score, secondary_contact_email: @admin_company.secondary_contact_email, secondary_contact_name: @admin_company.secondary_contact_name, service_complexities_while_offering: @admin_company.service_complexities_while_offering, service_description: @admin_company.service_description, service_end_date: @admin_company.service_end_date, service_start_date: @admin_company.service_start_date, social_context: @admin_company.social_context, sub_industry: @admin_company.sub_industry, technological_context: @admin_company.technological_context, updated_at: @admin_company.updated_at, user_id: @admin_company.user_id, work_environment_score: @admin_company.work_environment_score, workers_average_age: @admin_company.workers_average_age, workers_average_time_in_company: @admin_company.workers_average_time_in_company, workers_profile: @admin_company.workers_profile } }
    end

    assert_redirected_to admin_company_url(Admin::Company.last)
  end

  test "should show admin_company" do
    get admin_company_url(@admin_company)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_company_url(@admin_company)
    assert_response :success
  end

  test "should update admin_company" do
    patch admin_company_url(@admin_company), params: { admin_company: { cmp_business_plan: @admin_company.cmp_business_plan, cmp_changes_in_last_year: @admin_company.cmp_changes_in_last_year, cmp_competitors: @admin_company.cmp_competitors, cmp_hr_provider: @admin_company.cmp_hr_provider, cmp_mission: @admin_company.cmp_mission, cmp_name: @admin_company.cmp_name, cmp_organizational_structure: @admin_company.cmp_organizational_structure, cmp_size: @admin_company.cmp_size, cmp_value_proposition: @admin_company.cmp_value_proposition, cmp_values: @admin_company.cmp_values, cmp_vision: @admin_company.cmp_vision, complexity_in_relationship_with_client: @admin_company.complexity_in_relationship_with_client, conditions: @admin_company.conditions, consultant_charge: @admin_company.consultant_charge, contact_position: @admin_company.contact_position, created_at: @admin_company.created_at, date_of_service_provided: @admin_company.date_of_service_provided, economic_context: @admin_company.economic_context, environmental_context: @admin_company.environmental_context, geographical_region: @admin_company.geographical_region, geographical_state: @admin_company.geographical_state, industry: @admin_company.industry, integer: @admin_company.integer, is_holding: @admin_company.is_holding, last_service_provided_name: @admin_company.last_service_provided_name, legal_context: @admin_company.legal_context, number_of_workers: @admin_company.number_of_workers, parent_id: @admin_company.parent_id, political_context: @admin_company.political_context, position_of_primary_contact: @admin_company.position_of_primary_contact, primary_contact_cellphone: @admin_company.primary_contact_cellphone, primary_contact_email: @admin_company.primary_contact_email, primary_contact_name: @admin_company.primary_contact_name, primary_contact_phone: @admin_company.primary_contact_phone, relationship_with_client: @admin_company.relationship_with_client, score: @admin_company.score, secondary_contact_email: @admin_company.secondary_contact_email, secondary_contact_name: @admin_company.secondary_contact_name, service_complexities_while_offering: @admin_company.service_complexities_while_offering, service_description: @admin_company.service_description, service_end_date: @admin_company.service_end_date, service_start_date: @admin_company.service_start_date, social_context: @admin_company.social_context, sub_industry: @admin_company.sub_industry, technological_context: @admin_company.technological_context, updated_at: @admin_company.updated_at, user_id: @admin_company.user_id, work_environment_score: @admin_company.work_environment_score, workers_average_age: @admin_company.workers_average_age, workers_average_time_in_company: @admin_company.workers_average_time_in_company, workers_profile: @admin_company.workers_profile } }
    assert_redirected_to admin_company_url(@admin_company)
  end

  test "should destroy admin_company" do
    assert_difference('Admin::Company.count', -1) do
      delete admin_company_url(@admin_company)
    end

    assert_redirected_to admin_companies_url
  end
end
