require 'test_helper'

class Admin::ProcessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_process = admin_processes(:one)
  end

  test "should get index" do
    get admin_processes_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_process_url
    assert_response :success
  end

  test "should create admin_process" do
    assert_difference('Admin::Process.count') do
      post admin_processes_url, params: { admin_process: { name: @admin_process.name } }
    end

    assert_redirected_to admin_process_url(Admin::Process.last)
  end

  test "should show admin_process" do
    get admin_process_url(@admin_process)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_process_url(@admin_process)
    assert_response :success
  end

  test "should update admin_process" do
    patch admin_process_url(@admin_process), params: { admin_process: { name: @admin_process.name } }
    assert_redirected_to admin_process_url(@admin_process)
  end

  test "should destroy admin_process" do
    assert_difference('Admin::Process.count', -1) do
      delete admin_process_url(@admin_process)
    end

    assert_redirected_to admin_processes_url
  end
end
