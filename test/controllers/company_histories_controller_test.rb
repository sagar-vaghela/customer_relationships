require 'test_helper'

class CompanyHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @company_history = company_histories(:one)
  end

  test "should get index" do
    get company_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_company_history_url
    assert_response :success
  end

  test "should create company_history" do
    assert_difference('CompanyHistory.count') do
      post company_histories_url, params: { company_history: { cmp_business_plan: @company_history.cmp_business_plan, cmp_changes_in_last_year: @company_history.cmp_changes_in_last_year, cmp_competitors: @company_history.cmp_competitors, cmp_hr_provider: @company_history.cmp_hr_provider, cmp_mission: @company_history.cmp_mission, cmp_organizational_structure: @company_history.cmp_organizational_structure, cmp_size: @company_history.cmp_size, cmp_value_proposition: @company_history.cmp_value_proposition, cmp_values: @company_history.cmp_values, cmp_vision: @company_history.cmp_vision, company: @company_history.company, complexity_in_relationship_with_client: @company_history.complexity_in_relationship_with_client, conditions: @company_history.conditions, consultant_charge: @company_history.consultant_charge, contact_position: @company_history.contact_position, date_of_service_provided: @company_history.date_of_service_provided, economic_context: @company_history.economic_context, environmental_context: @company_history.environmental_context, geographical_region: @company_history.geographical_region, geographical_state: @company_history.geographical_state, last_service_provided_name: @company_history.last_service_provided_name, legal_context: @company_history.legal_context, number_of_workers: @company_history.number_of_workers, political_context: @company_history.political_context, position_of_primary_contact: @company_history.position_of_primary_contact, primary_contact_cellphone: @company_history.primary_contact_cellphone, primary_contact_email: @company_history.primary_contact_email, primary_contact_name: @company_history.primary_contact_name, primary_contact_phone: @company_history.primary_contact_phone, relationship_with_client: @company_history.relationship_with_client, score: @company_history.score, secondary_contact_email: @company_history.secondary_contact_email, secondary_contact_name: @company_history.secondary_contact_name, service_complexities_while_offering: @company_history.service_complexities_while_offering, service_description: @company_history.service_description, service_end_date: @company_history.service_end_date, service_start_date: @company_history.service_start_date, social_context: @company_history.social_context, technological_context: @company_history.technological_context, user: @company_history.user, work_environment_score: @company_history.work_environment_score, workers_average_age: @company_history.workers_average_age, workers_average_time_in_company: @company_history.workers_average_time_in_company, workers_profile: @company_history.workers_profile, year: @company_history.year } }
    end

    assert_redirected_to company_history_url(CompanyHistory.last)
  end

  test "should show company_history" do
    get company_history_url(@company_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_company_history_url(@company_history)
    assert_response :success
  end

  test "should update company_history" do
    patch company_history_url(@company_history), params: { company_history: { cmp_business_plan: @company_history.cmp_business_plan, cmp_changes_in_last_year: @company_history.cmp_changes_in_last_year, cmp_competitors: @company_history.cmp_competitors, cmp_hr_provider: @company_history.cmp_hr_provider, cmp_mission: @company_history.cmp_mission, cmp_organizational_structure: @company_history.cmp_organizational_structure, cmp_size: @company_history.cmp_size, cmp_value_proposition: @company_history.cmp_value_proposition, cmp_values: @company_history.cmp_values, cmp_vision: @company_history.cmp_vision, company: @company_history.company, complexity_in_relationship_with_client: @company_history.complexity_in_relationship_with_client, conditions: @company_history.conditions, consultant_charge: @company_history.consultant_charge, contact_position: @company_history.contact_position, date_of_service_provided: @company_history.date_of_service_provided, economic_context: @company_history.economic_context, environmental_context: @company_history.environmental_context, geographical_region: @company_history.geographical_region, geographical_state: @company_history.geographical_state, last_service_provided_name: @company_history.last_service_provided_name, legal_context: @company_history.legal_context, number_of_workers: @company_history.number_of_workers, political_context: @company_history.political_context, position_of_primary_contact: @company_history.position_of_primary_contact, primary_contact_cellphone: @company_history.primary_contact_cellphone, primary_contact_email: @company_history.primary_contact_email, primary_contact_name: @company_history.primary_contact_name, primary_contact_phone: @company_history.primary_contact_phone, relationship_with_client: @company_history.relationship_with_client, score: @company_history.score, secondary_contact_email: @company_history.secondary_contact_email, secondary_contact_name: @company_history.secondary_contact_name, service_complexities_while_offering: @company_history.service_complexities_while_offering, service_description: @company_history.service_description, service_end_date: @company_history.service_end_date, service_start_date: @company_history.service_start_date, social_context: @company_history.social_context, technological_context: @company_history.technological_context, user: @company_history.user, work_environment_score: @company_history.work_environment_score, workers_average_age: @company_history.workers_average_age, workers_average_time_in_company: @company_history.workers_average_time_in_company, workers_profile: @company_history.workers_profile, year: @company_history.year } }
    assert_redirected_to company_history_url(@company_history)
  end

  test "should destroy company_history" do
    assert_difference('CompanyHistory.count', -1) do
      delete company_history_url(@company_history)
    end

    assert_redirected_to company_histories_url
  end
end
