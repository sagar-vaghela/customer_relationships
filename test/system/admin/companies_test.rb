require "application_system_test_case"

class Admin::CompaniesTest < ApplicationSystemTestCase
  setup do
    @admin_company = admin_companies(:one)
  end

  test "visiting the index" do
    visit admin_companies_url
    assert_selector "h1", text: "Admin/Companies"
  end

  test "creating a Company" do
    visit admin_companies_url
    click_on "New Admin/Company"

    fill_in "Cmp Business Plan", with: @admin_company.cmp_business_plan
    fill_in "Cmp Changes In Last Year", with: @admin_company.cmp_changes_in_last_year
    fill_in "Cmp Competitors", with: @admin_company.cmp_competitors
    fill_in "Cmp Hr Provider", with: @admin_company.cmp_hr_provider
    fill_in "Cmp Mission", with: @admin_company.cmp_mission
    fill_in "Cmp Name", with: @admin_company.cmp_name
    fill_in "Cmp Organizational Structure", with: @admin_company.cmp_organizational_structure
    fill_in "Cmp Size", with: @admin_company.cmp_size
    fill_in "Cmp Value Proposition", with: @admin_company.cmp_value_proposition
    fill_in "Cmp Values", with: @admin_company.cmp_values
    fill_in "Cmp Vision", with: @admin_company.cmp_vision
    fill_in "Complexity In Relationship With Client", with: @admin_company.complexity_in_relationship_with_client
    fill_in "Conditions", with: @admin_company.conditions
    fill_in "Consultant Charge", with: @admin_company.consultant_charge
    fill_in "Contact Position", with: @admin_company.contact_position
    fill_in "Created At", with: @admin_company.created_at
    fill_in "Date Of Service Provided", with: @admin_company.date_of_service_provided
    fill_in "Economic Context", with: @admin_company.economic_context
    fill_in "Environmental Context", with: @admin_company.environmental_context
    fill_in "Geographical Region", with: @admin_company.geographical_region
    fill_in "Geographical State", with: @admin_company.geographical_state
    fill_in "Industry", with: @admin_company.industry
    fill_in "Integer", with: @admin_company.integer
    fill_in "Is Holding", with: @admin_company.is_holding
    fill_in "Last Service Provided Name", with: @admin_company.last_service_provided_name
    fill_in "Legal Context", with: @admin_company.legal_context
    fill_in "Number Of Workers", with: @admin_company.number_of_workers
    fill_in "Parent", with: @admin_company.parent_id
    fill_in "Political Context", with: @admin_company.political_context
    fill_in "Position Of Primary Contact", with: @admin_company.position_of_primary_contact
    fill_in "Primary Contact Cellphone", with: @admin_company.primary_contact_cellphone
    fill_in "Primary Contact Email", with: @admin_company.primary_contact_email
    fill_in "Primary Contact Name", with: @admin_company.primary_contact_name
    fill_in "Primary Contact Phone", with: @admin_company.primary_contact_phone
    fill_in "Relationship With Client", with: @admin_company.relationship_with_client
    fill_in "Score", with: @admin_company.score
    fill_in "Secondary Contact Email", with: @admin_company.secondary_contact_email
    fill_in "Secondary Contact Name", with: @admin_company.secondary_contact_name
    fill_in "Service Complexities While Offering", with: @admin_company.service_complexities_while_offering
    fill_in "Service Description", with: @admin_company.service_description
    fill_in "Service End Date", with: @admin_company.service_end_date
    fill_in "Service Start Date", with: @admin_company.service_start_date
    fill_in "Social Context", with: @admin_company.social_context
    fill_in "Sub Industry", with: @admin_company.sub_industry
    fill_in "Technological Context", with: @admin_company.technological_context
    fill_in "Updated At", with: @admin_company.updated_at
    fill_in "User", with: @admin_company.user_id
    fill_in "Work Environment Score", with: @admin_company.work_environment_score
    fill_in "Workers Average Age", with: @admin_company.workers_average_age
    fill_in "Workers Average Time In Company", with: @admin_company.workers_average_time_in_company
    fill_in "Workers Profile", with: @admin_company.workers_profile
    click_on "Create Company"

    assert_text "Company was successfully created"
    click_on "Back"
  end

  test "updating a Company" do
    visit admin_companies_url
    click_on "Edit", match: :first

    fill_in "Cmp Business Plan", with: @admin_company.cmp_business_plan
    fill_in "Cmp Changes In Last Year", with: @admin_company.cmp_changes_in_last_year
    fill_in "Cmp Competitors", with: @admin_company.cmp_competitors
    fill_in "Cmp Hr Provider", with: @admin_company.cmp_hr_provider
    fill_in "Cmp Mission", with: @admin_company.cmp_mission
    fill_in "Cmp Name", with: @admin_company.cmp_name
    fill_in "Cmp Organizational Structure", with: @admin_company.cmp_organizational_structure
    fill_in "Cmp Size", with: @admin_company.cmp_size
    fill_in "Cmp Value Proposition", with: @admin_company.cmp_value_proposition
    fill_in "Cmp Values", with: @admin_company.cmp_values
    fill_in "Cmp Vision", with: @admin_company.cmp_vision
    fill_in "Complexity In Relationship With Client", with: @admin_company.complexity_in_relationship_with_client
    fill_in "Conditions", with: @admin_company.conditions
    fill_in "Consultant Charge", with: @admin_company.consultant_charge
    fill_in "Contact Position", with: @admin_company.contact_position
    fill_in "Created At", with: @admin_company.created_at
    fill_in "Date Of Service Provided", with: @admin_company.date_of_service_provided
    fill_in "Economic Context", with: @admin_company.economic_context
    fill_in "Environmental Context", with: @admin_company.environmental_context
    fill_in "Geographical Region", with: @admin_company.geographical_region
    fill_in "Geographical State", with: @admin_company.geographical_state
    fill_in "Industry", with: @admin_company.industry
    fill_in "Integer", with: @admin_company.integer
    fill_in "Is Holding", with: @admin_company.is_holding
    fill_in "Last Service Provided Name", with: @admin_company.last_service_provided_name
    fill_in "Legal Context", with: @admin_company.legal_context
    fill_in "Number Of Workers", with: @admin_company.number_of_workers
    fill_in "Parent", with: @admin_company.parent_id
    fill_in "Political Context", with: @admin_company.political_context
    fill_in "Position Of Primary Contact", with: @admin_company.position_of_primary_contact
    fill_in "Primary Contact Cellphone", with: @admin_company.primary_contact_cellphone
    fill_in "Primary Contact Email", with: @admin_company.primary_contact_email
    fill_in "Primary Contact Name", with: @admin_company.primary_contact_name
    fill_in "Primary Contact Phone", with: @admin_company.primary_contact_phone
    fill_in "Relationship With Client", with: @admin_company.relationship_with_client
    fill_in "Score", with: @admin_company.score
    fill_in "Secondary Contact Email", with: @admin_company.secondary_contact_email
    fill_in "Secondary Contact Name", with: @admin_company.secondary_contact_name
    fill_in "Service Complexities While Offering", with: @admin_company.service_complexities_while_offering
    fill_in "Service Description", with: @admin_company.service_description
    fill_in "Service End Date", with: @admin_company.service_end_date
    fill_in "Service Start Date", with: @admin_company.service_start_date
    fill_in "Social Context", with: @admin_company.social_context
    fill_in "Sub Industry", with: @admin_company.sub_industry
    fill_in "Technological Context", with: @admin_company.technological_context
    fill_in "Updated At", with: @admin_company.updated_at
    fill_in "User", with: @admin_company.user_id
    fill_in "Work Environment Score", with: @admin_company.work_environment_score
    fill_in "Workers Average Age", with: @admin_company.workers_average_age
    fill_in "Workers Average Time In Company", with: @admin_company.workers_average_time_in_company
    fill_in "Workers Profile", with: @admin_company.workers_profile
    click_on "Update Company"

    assert_text "Company was successfully updated"
    click_on "Back"
  end

  test "destroying a Company" do
    visit admin_companies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Company was successfully destroyed"
  end
end
