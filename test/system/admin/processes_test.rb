require "application_system_test_case"

class Admin::ProcessesTest < ApplicationSystemTestCase
  setup do
    @admin_process = admin_processes(:one)
  end

  test "visiting the index" do
    visit admin_processes_url
    assert_selector "h1", text: "Admin/Processes"
  end

  test "creating a Process" do
    visit admin_processes_url
    click_on "New Admin/Process"

    fill_in "Name", with: @admin_process.name
    click_on "Create Process"

    assert_text "Process was successfully created"
    click_on "Back"
  end

  test "updating a Process" do
    visit admin_processes_url
    click_on "Edit", match: :first

    fill_in "Name", with: @admin_process.name
    click_on "Update Process"

    assert_text "Process was successfully updated"
    click_on "Back"
  end

  test "destroying a Process" do
    visit admin_processes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Process was successfully destroyed"
  end
end
