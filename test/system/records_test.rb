require "application_system_test_case"

class RecordsTest < ApplicationSystemTestCase
  setup do
    @record = records(:one)
  end

  test "visiting the index" do
    visit records_url
    assert_selector "h1", text: "Records"
  end

  test "creating a Record" do
    visit records_url
    click_on "New Record"

    fill_in "Affiliates In City", with: @record.affiliates_in_city
    fill_in "Affiliates In Country", with: @record.affiliates_in_country
    fill_in "Affiliates In State", with: @record.affiliates_in_state
    fill_in "Bill Receiver Name", with: @record.bill_receiver_name
    fill_in "Bill Receiver Phone", with: @record.bill_receiver_phone
    fill_in "Chilean Id Number", with: @record.chilean_id_number
    fill_in "Cmp Address", with: @record.cmp_address
    fill_in "Cmp Business Plan", with: @record.cmp_business_plan
    fill_in "Cmp City", with: @record.cmp_city
    fill_in "Cmp Fax", with: @record.cmp_fax
    fill_in "Cmp General Managers Name", with: @record.cmp_general_managers_name
    fill_in "Cmp Hrm Email", with: @record.cmp_hrm_email
    fill_in "Cmp Hrm Name", with: @record.cmp_hrm_name
    fill_in "Cmp Name", with: @record.cmp_name
    fill_in "Cmp Number", with: @record.cmp_number
    fill_in "Cmp Office", with: @record.cmp_office
    fill_in "Cmp Phone", with: @record.cmp_phone
    fill_in "Cmp Phone Number", with: @record.cmp_phone_number
    fill_in "Cmp State", with: @record.cmp_state
    fill_in "Company", with: @record.company_id
    fill_in "Date Of Service Provided", with: @record.date_of_service_provided
    fill_in "Description Of Service", with: @record.description_of_service
    fill_in "Number Of Employees", with: @record.number_of_employees
    fill_in "Number Of Segments", with: @record.number_of_segments
    fill_in "Official Name Of Client", with: @record.official_name_of_client
    fill_in "Primary Contact Person Email", with: @record.primary_contact_person_email
    fill_in "Primary Contact Person Name", with: @record.primary_contact_person_name
    fill_in "Primary Contact Person Phone", with: @record.primary_contact_person_phone
    fill_in "Primary Contact Person Role", with: @record.primary_contact_person_role
    fill_in "Service Duration", with: @record.service_duration
    fill_in "Service Type", with: @record.service_type
    fill_in "State", with: @record.state
    fill_in "Survey Mode", with: @record.survey_mode
    fill_in "User", with: @record.user_id
    fill_in "Value Of Service", with: @record.value_of_service
    click_on "Create Record"

    assert_text "Record was successfully created"
    click_on "Back"
  end

  test "updating a Record" do
    visit records_url
    click_on "Edit", match: :first

    fill_in "Affiliates In City", with: @record.affiliates_in_city
    fill_in "Affiliates In Country", with: @record.affiliates_in_country
    fill_in "Affiliates In State", with: @record.affiliates_in_state
    fill_in "Bill Receiver Name", with: @record.bill_receiver_name
    fill_in "Bill Receiver Phone", with: @record.bill_receiver_phone
    fill_in "Chilean Id Number", with: @record.chilean_id_number
    fill_in "Cmp Address", with: @record.cmp_address
    fill_in "Cmp Business Plan", with: @record.cmp_business_plan
    fill_in "Cmp City", with: @record.cmp_city
    fill_in "Cmp Fax", with: @record.cmp_fax
    fill_in "Cmp General Managers Name", with: @record.cmp_general_managers_name
    fill_in "Cmp Hrm Email", with: @record.cmp_hrm_email
    fill_in "Cmp Hrm Name", with: @record.cmp_hrm_name
    fill_in "Cmp Name", with: @record.cmp_name
    fill_in "Cmp Number", with: @record.cmp_number
    fill_in "Cmp Office", with: @record.cmp_office
    fill_in "Cmp Phone", with: @record.cmp_phone
    fill_in "Cmp Phone Number", with: @record.cmp_phone_number
    fill_in "Cmp State", with: @record.cmp_state
    fill_in "Company", with: @record.company_id
    fill_in "Date Of Service Provided", with: @record.date_of_service_provided
    fill_in "Description Of Service", with: @record.description_of_service
    fill_in "Number Of Employees", with: @record.number_of_employees
    fill_in "Number Of Segments", with: @record.number_of_segments
    fill_in "Official Name Of Client", with: @record.official_name_of_client
    fill_in "Primary Contact Person Email", with: @record.primary_contact_person_email
    fill_in "Primary Contact Person Name", with: @record.primary_contact_person_name
    fill_in "Primary Contact Person Phone", with: @record.primary_contact_person_phone
    fill_in "Primary Contact Person Role", with: @record.primary_contact_person_role
    fill_in "Service Duration", with: @record.service_duration
    fill_in "Service Type", with: @record.service_type
    fill_in "State", with: @record.state
    fill_in "Survey Mode", with: @record.survey_mode
    fill_in "User", with: @record.user_id
    fill_in "Value Of Service", with: @record.value_of_service
    click_on "Update Record"

    assert_text "Record was successfully updated"
    click_on "Back"
  end

  test "destroying a Record" do
    visit records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Record was successfully destroyed"
  end
end
