require "application_system_test_case"

class CompanyHistoriesTest < ApplicationSystemTestCase
  setup do
    @company_history = company_histories(:one)
  end

  test "visiting the index" do
    visit company_histories_url
    assert_selector "h1", text: "Company Histories"
  end

  test "creating a Company history" do
    visit company_histories_url
    click_on "New Company History"

    fill_in "Cmp Business Plan", with: @company_history.cmp_business_plan
    fill_in "Cmp Changes In Last Year", with: @company_history.cmp_changes_in_last_year
    fill_in "Cmp Competitors", with: @company_history.cmp_competitors
    fill_in "Cmp Hr Provider", with: @company_history.cmp_hr_provider
    fill_in "Cmp Mission", with: @company_history.cmp_mission
    fill_in "Cmp Organizational Structure", with: @company_history.cmp_organizational_structure
    fill_in "Cmp Size", with: @company_history.cmp_size
    fill_in "Cmp Value Proposition", with: @company_history.cmp_value_proposition
    fill_in "Cmp Values", with: @company_history.cmp_values
    fill_in "Cmp Vision", with: @company_history.cmp_vision
    fill_in "Company", with: @company_history.company
    fill_in "Complexity In Relationship With Client", with: @company_history.complexity_in_relationship_with_client
    fill_in "Conditions", with: @company_history.conditions
    fill_in "Consultant Charge", with: @company_history.consultant_charge
    fill_in "Contact Position", with: @company_history.contact_position
    fill_in "Date Of Service Provided", with: @company_history.date_of_service_provided
    fill_in "Economic Context", with: @company_history.economic_context
    fill_in "Environmental Context", with: @company_history.environmental_context
    fill_in "Geographical Region", with: @company_history.geographical_region
    fill_in "Geographical State", with: @company_history.geographical_state
    fill_in "Last Service Provided Name", with: @company_history.last_service_provided_name
    fill_in "Legal Context", with: @company_history.legal_context
    fill_in "Number Of Workers", with: @company_history.number_of_workers
    fill_in "Political Context", with: @company_history.political_context
    fill_in "Position Of Primary Contact", with: @company_history.position_of_primary_contact
    fill_in "Primary Contact Cellphone", with: @company_history.primary_contact_cellphone
    fill_in "Primary Contact Email", with: @company_history.primary_contact_email
    fill_in "Primary Contact Name", with: @company_history.primary_contact_name
    fill_in "Primary Contact Phone", with: @company_history.primary_contact_phone
    fill_in "Relationship With Client", with: @company_history.relationship_with_client
    fill_in "Score", with: @company_history.score
    fill_in "Secondary Contact Email", with: @company_history.secondary_contact_email
    fill_in "Secondary Contact Name", with: @company_history.secondary_contact_name
    fill_in "Service Complexities While Offering", with: @company_history.service_complexities_while_offering
    fill_in "Service Description", with: @company_history.service_description
    fill_in "Service End Date", with: @company_history.service_end_date
    fill_in "Service Start Date", with: @company_history.service_start_date
    fill_in "Social Context", with: @company_history.social_context
    fill_in "Technological Context", with: @company_history.technological_context
    fill_in "User", with: @company_history.user
    fill_in "Work Environment Score", with: @company_history.work_environment_score
    fill_in "Workers Average Age", with: @company_history.workers_average_age
    fill_in "Workers Average Time In Company", with: @company_history.workers_average_time_in_company
    fill_in "Workers Profile", with: @company_history.workers_profile
    fill_in "Year", with: @company_history.year
    click_on "Create Company history"

    assert_text "Company history was successfully created"
    click_on "Back"
  end

  test "updating a Company history" do
    visit company_histories_url
    click_on "Edit", match: :first

    fill_in "Cmp Business Plan", with: @company_history.cmp_business_plan
    fill_in "Cmp Changes In Last Year", with: @company_history.cmp_changes_in_last_year
    fill_in "Cmp Competitors", with: @company_history.cmp_competitors
    fill_in "Cmp Hr Provider", with: @company_history.cmp_hr_provider
    fill_in "Cmp Mission", with: @company_history.cmp_mission
    fill_in "Cmp Organizational Structure", with: @company_history.cmp_organizational_structure
    fill_in "Cmp Size", with: @company_history.cmp_size
    fill_in "Cmp Value Proposition", with: @company_history.cmp_value_proposition
    fill_in "Cmp Values", with: @company_history.cmp_values
    fill_in "Cmp Vision", with: @company_history.cmp_vision
    fill_in "Company", with: @company_history.company
    fill_in "Complexity In Relationship With Client", with: @company_history.complexity_in_relationship_with_client
    fill_in "Conditions", with: @company_history.conditions
    fill_in "Consultant Charge", with: @company_history.consultant_charge
    fill_in "Contact Position", with: @company_history.contact_position
    fill_in "Date Of Service Provided", with: @company_history.date_of_service_provided
    fill_in "Economic Context", with: @company_history.economic_context
    fill_in "Environmental Context", with: @company_history.environmental_context
    fill_in "Geographical Region", with: @company_history.geographical_region
    fill_in "Geographical State", with: @company_history.geographical_state
    fill_in "Last Service Provided Name", with: @company_history.last_service_provided_name
    fill_in "Legal Context", with: @company_history.legal_context
    fill_in "Number Of Workers", with: @company_history.number_of_workers
    fill_in "Political Context", with: @company_history.political_context
    fill_in "Position Of Primary Contact", with: @company_history.position_of_primary_contact
    fill_in "Primary Contact Cellphone", with: @company_history.primary_contact_cellphone
    fill_in "Primary Contact Email", with: @company_history.primary_contact_email
    fill_in "Primary Contact Name", with: @company_history.primary_contact_name
    fill_in "Primary Contact Phone", with: @company_history.primary_contact_phone
    fill_in "Relationship With Client", with: @company_history.relationship_with_client
    fill_in "Score", with: @company_history.score
    fill_in "Secondary Contact Email", with: @company_history.secondary_contact_email
    fill_in "Secondary Contact Name", with: @company_history.secondary_contact_name
    fill_in "Service Complexities While Offering", with: @company_history.service_complexities_while_offering
    fill_in "Service Description", with: @company_history.service_description
    fill_in "Service End Date", with: @company_history.service_end_date
    fill_in "Service Start Date", with: @company_history.service_start_date
    fill_in "Social Context", with: @company_history.social_context
    fill_in "Technological Context", with: @company_history.technological_context
    fill_in "User", with: @company_history.user
    fill_in "Work Environment Score", with: @company_history.work_environment_score
    fill_in "Workers Average Age", with: @company_history.workers_average_age
    fill_in "Workers Average Time In Company", with: @company_history.workers_average_time_in_company
    fill_in "Workers Profile", with: @company_history.workers_profile
    fill_in "Year", with: @company_history.year
    click_on "Update Company history"

    assert_text "Company history was successfully updated"
    click_on "Back"
  end

  test "destroying a Company history" do
    visit company_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Company history was successfully destroyed"
  end
end
